<?
define("BX_DONT_SKIP_PULL_INIT", true);
require($_SERVER["DOCUMENT_ROOT"]."/desktop_app/headers.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
if (intval($USER->GetID()) <= 0) return;
IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/im/install/public/desktop_app/index.php");
?>
<div class="bx-desktop">
	<div class="bx-desktop-links" id="bx-desktop-links"><a href="#logout" onclick="return BXIM.desktop.logout();" class="bx-desktop-link"><?=GetMessage('DESKTOP_LOGOUT')?></a></div>
	<div class="bx-desktop-tabs" id="bx-desktop-tabs"><span class="bx-desktop-tab bx-desktop-tab-active" onclick="BXIM.desktop.changeTab(this)"><?=GetMessage('DESKTOP_TAB_IM')?></span><span><a href="javascript:BXIM.desktop.openLF()" class="bx-desktop-link"><?=GetMessage('DESKTOP_TAB_LF')?></a><span id="bx-desktop-tab-lf-count"></span></span></div>
	<div class="bx-desktop-contents" id="bx-desktop-contents">
		<div class="bx-desktop-content" data-page="im"><div id="placeholder-messenger" class="placeholder-messenger"></div></div><?/*
		<div class="bx-desktop-content bx-desktop-content-hide bx-desktop-content-overflow" data-page="lf">
			<div id="placeholder-lf" class="placeholder-lf">
				<?$APPLICATION->IncludeComponent("bitrix:socialnetwork.log", ".default", array(
						"GROUP_ID" => intval($_GET["group_id"]),
						"LOG_ID" => intval($_GET["detail_log_id"]),
						"SET_LOG_CACHE" => "Y",
						"IMAGE_MAX_WIDTH" => 550,
						"DATE_TIME_FORMAT" => ((intval($_GET["detail_log_id"]) > 0 || $_REQUEST["ACTION"] == "CONVERT") ? "j F Y G:i" : ""),
						"AJAX_MODE" => "Y"
					),
					false
				);?>
			</div>
		</div>*/?>
	</div>
</div>
<?$APPLICATION->IncludeComponent("bitrix:im.messenger", "", Array("DESKTOP" => "Y"), false, Array("HIDE_ICONS" => "Y"));?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php")?>